# Appeel test

## Test

### Wat wordt er verwacht

Een app die verbinding maakt met de Github API en alle openbare repositories van een gebruiker weergeeft, en de laatste commits voor de repositories. 

### Taken

1. Maak verbinding met de Github API
    - Maak verbinding met de Github API en maak een lijst van alle openbare opslagplaatsen van uw gebruiker.
    - Maak een pagina met de openbare opslagplaatsen.
2. Laad commits
    - Nadat op een repository is geklikt, moet de gebruiker een pagina te zien krijgen met de laatste 20 commits op die repository, ook een zoekveld om de commits te filteren op de term die in het veld is ingevoegd.

Het staat je vrij om te kiezen of je de REST API of de GraphQL API gaat gebruiken, en welke subset van informatie je op elke pagina wilt laten zien.

### Bonustaken

- Gebruik een moderne CSS-oplossing (CSS-modules, Styled-componenten, enz.);
- Infinite scroll voor de commits-pagina;
- Maak het mogelijk om de volgorde waarin de repositories worden weergegeven te wijzigen (met sterren, naam, enz.);
- Component library (Storybook.js, Styleguidist, enz.);
- Server-side rendering.
 

### Oplossing

- Bepaal welk framework je wil gebruiken: VueJS, ReactJS of Angular;
    - Met VueJS kun je beginnen met een eenvoudige Vue CLI-app en van daaruit verder werken;
    - Met ReactJS kun je beginnen met een eenvoudige create-react-app structuur en van daaruit verder werken;
    - Met Angular kun je beginnen met een eenvoudige Angular CLI-app structuur en van daaruit verder werken;
- Gebruik een oplossing voor state management;
- Gebruik ES6+ -functies en noteer in het README-bestand waarom en waarvoor, voor minimaal 2 ervan;
- Schrijf unit-tests en andere tests die je mogelijk nuttig of belangrijk vindt;
- Houd rekening met prestaties en edge cases;
- Wees creatief en laat ons zien wat je in huis hebt.
 

### Oplevering

- Maak een nieuwe repository in je account en stuur ons de URL;
- Maak deze repository publiek, of voeg username "wiven" toe als member;
- Maak een README-bestand met de instructies om het project en de tests uit te voeren, en voeg commentaar toe waarvan je denkt dat het relevant is.

## Instructies

Dit project is een React applicatie en gebruikt create-react-app. Ga naar de web map en je kan deze opstarten met `npm run start`. Project kan ook [live bekeken worden](https://naseki-appeel-test.netlify.app).

## Testing

Momenteel is er 1 test aangemaakt met Cypress die algemeen de React app test. Ga naar de web map en je kan de test opstarten met `npm run test`. Cypress wordt hiermee opgestart en je krijgt een nieuw venster. Je kan dan de test runnen die ik heb geschreven in `actual-tests/default.spec.js`.

## ES6+ features

- */web/src/views/Repository/Repository.js* - **Template literals** - ``https://api.github.com/repos/${userRoute}/${repoRoute}/commits?per_page=20&page=${page+1}`` - Een erg snelle en clean manier om strings op te bouwen met veel variabelen of meerdere lijnen. Zorgt niet er niet alleen voor dat het coderen van zulke strings worden versneld, maar ook voor verbeterde leesbaarheid.
- */web/src/views/Repository/Repository.js* - **Spread syntax** - `[...prev, ...data]` - Spread syntax vult een array aan met de opgenoemde array zijn values. Het werkt op een gelijkaardige manier als `array.apply()`. Buiten betere leesbaarheid heeft de spread syntax ook betere performance tegenover apply en concat, maar blijft wel vergelijkbaar met push.
- */web/src/components/profile/Repo.js* - **Array functions** - `.filter(...).sort(...).map(...)` - Array functions zijn meestal beter dan zelf je eigen algoritmes te schrijven, aangezien de algoritmes die worden toegepast al optimaal zijn. Dit bespaart natuurlijk ook heel erg veel tijd.
- */web/src/views/Profile/Profile.js* - **const + let** - `const [ buttonoff, setButtonoff ] = useState(false); let { userRoute, repoRoute } = useParams();` - Const en let zijn twee nieuwe manier om een variabele te definiëren in JS. Const laat je toe om constanten te definiëren, wat handig is om zeker te zijn dat je niets kunt aanpassen bij deze variabelen. Let heeft een soortgelijke functie als var buiten dat de scope van de variabele heel anders werkt. Let variabelen blijven in de block scope en kunnen niet gebruikt worden voor ze zijn gedefiniëerd.
- */web/src/components/global/Nav.js* - **Arrow functions** - `user => history.push('/' + user)` - Arrow functions verkorten functies en gebruik ik vooral wanneer het om kleine functies gaat en dat ik `this` niet nodig heb.