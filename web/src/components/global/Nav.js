import { Link, useHistory } from "react-router-dom";

import Search from '../form-fields/Search';

function Nav() {
    let history = useHistory();

    const handleUserSearch = user => history.push('/' + user);

    return (
      <nav className="nav">
          <div className="nav__logo"><Link to="/">Prettier GitHub</Link></div>
          <div className="nav__search"><Search size="small" placeholder="Search GitHub user" handleSubmit={handleUserSearch} /></div>
      </nav>
    );
}

export default Nav;