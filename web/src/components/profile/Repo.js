import { useState } from 'react';
import { Link } from "react-router-dom";

import Sort from '../form-fields/Sort'

import time from '../../assets/images/time-black.svg';
import star from '../../assets/images/star.svg';

function Repo(props) {
    // 0 = default (last updated), 1 = name, 2 = stars
    const [ sortmethod, setSortmethod ] = useState(0);

    const sortings = [
        (repo1, repo2) => new Date(repo2.updated_at) - new Date(repo1.updated_at),
        (repo1, repo2) => repo1.name - repo2.name,
        (repo1, repo2) => repo2.stargazers_count - repo1.stargazers_count
    ]

    // TODO add sort
    return (
      <div className="repository-list">
          <div className="repository-list__header">
              <div className="repostiroy-list__title"><h2>Repositories</h2></div>
              <div className="repostiroy-list__sort"><Sort sort={['Last updated', 'Alphabethical', 'Stars']} setsort={setSortmethod} /></div>
          </div>

          <div className="repository-list__grid">
            {props.data
              .filter(repo => !repo.private)
              .sort((repo1, repo2) => sortings[sortmethod](repo1, repo2))
              .map(repo => 
                  <Link to={`/${repo.owner.login}/${repo.name}`}>
                      <div className="repository-list__single">
                          <div className="repository-list__single__main">
                              <span className="repository-list__single__title">{repo.name}</span>
                              <span className={'repository-list__single__lang mark--' + repo.language}>{repo.language || 'Language not specified'}</span>
                          </div>
                          <div className="repository-list__single__bottom">
                          <span className="repository-list__single__lastupdated"><img src={time} alt="last updated at" />{new Date(repo.updated_at).toLocaleDateString('en-UK', { year: 'numeric', month: 'short', day: 'numeric' })}</span>
                          <span className="repository-list__single__stars"><img src={star} alt="stargazers count" />{repo.stargazers_count}</span>
                          </div>
                      </div>
                  </Link>
              )
            }
          </div>
      </div>
    );
}

export default Repo;