function Icon(props) {
    return (
      <div className={'icon' + (props.size === 'big' ? ' icon--big' : ' icon--small')}><img src={props.img} alt={props.alt} /></div>
    );
}

export default Icon;