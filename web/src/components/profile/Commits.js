import { useState } from 'react';
import { Link } from "react-router-dom";

import Icon from './Icon';
import Search from '../form-fields/Search';

import time from '../../assets/images/time.svg';

function Commits(props) {
    const [ filterterm, setFilterterm ] = useState('');

    const handleCommitSearch = term => setFilterterm(term);

    return (
      <div className="commits">
            <div className="commits__header">
              <div className="commits__header__title"><h2>Commits</h2></div>
              <div className="commits__header__search"><Search size="big" placeholder="Search commit" handleSubmit={handleCommitSearch} /></div>
            </div>

            {!filterterm ? '' : 
                <div className="commits__searchresults">
                    <div className="commits__searchresults__result">Results for: {filterterm} (only checks the 20 latest commits)</div>
                    <div className="commits__searchresults__close" onClick={() => setFilterterm('')}>x remove search</div>
                </div>
            }
    
            <div className="commits__list">
                {props.data
                    .filter((commit, index) => !filterterm ? commit : index < 20 && commit.commit.message.toLowerCase().includes(filterterm.toLowerCase()))
                    .map(commit => 
                        <div className="commits__single">
                            <div className="commits__single__desc">{commit.commit.message}</div>
                            <div className="commits__single__side">
                                <div className="commits__single__author">{commit.author ? <Link to={'/' + commit.author.login.replace('[bot]', '')}><Icon size="small" img={commit.author.avatar_url} alt={commit.author.login} /> <span>{commit.author.login.replace('[bot]', '')}</span></Link> : 'No username'}</div>
                                <div className="commits__single__date"><img src={time} alt="last updated at" />{new Date(commit.commit.author.date).toLocaleDateString('en-UK', { year: 'numeric', month: 'short', day: 'numeric' })}</div>
                            </div>
                        </div>
                    )   
                }

                {!filterterm && !props.buttonoff ? <button className="commits__loadmore" onClick={props.load}>Load more commits</button> : ''}
            </div>
      </div>
    );
}

export default Commits;