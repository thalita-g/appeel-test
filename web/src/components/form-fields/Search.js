import React, { useState } from 'react';

function Search(props) {
    const [ input, setInput ] = useState('');

    const handleInput = e => setInput(e.target.value);

    const handleSubmit = function() {
      if (input) {
        props.handleSubmit(input)
        setInput('');
      }
    }

    return (
      <div className={"search" + (props.size === 'big' ? ' search--big' : ' search--small')}>
          <input className="search__input" type="text" placeholder={props.placeholder} value={input} onChange={handleInput} onKeyUp={e => { if (e.key === 'Enter') { handleSubmit() } }} />
          <button className="search__button" onClick={handleSubmit}>Search</button>
      </div>
    );
  }
  
  export default Search;