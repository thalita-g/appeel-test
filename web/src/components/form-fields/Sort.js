import React, { useState } from 'react';

function Sort(props) {
    const [ input, setInput ] = useState(0);
    const [ open, setOpen ] = useState(false);

    const selectItem = function(index) {
        setInput(index);
        setOpen(false);
        props.setsort(index)
    }

    return (
      <div className="sort">
            <div className="sort__base" onClick={() => setOpen(prev => !prev)}>Sort {open ? '^' : 'v'}</div>
            {open && <div className="sort__dropdown">
                {props.sort.map((type, index) => <div className={'sort__dropdown__item' + (index === input ? ' sort__dropdown__item--active' : '')} onClick={() => selectItem(index)}>{type}</div> )}
            </div>}
      </div>
    );
  }
  
  export default Sort;