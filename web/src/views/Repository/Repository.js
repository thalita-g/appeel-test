import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from "react-router-dom";

import Nav from '../../components/global/Nav';
import Commits from '../../components/profile/Commits'

function Repository() {
    const [ fetchstatus, setFetchstatus ] = useState(true);
    const [ page, setPage ] = useState(1);
    const [ commits, setCommits ] = useState([]);
    const [ buttonoff, setButtonoff ] = useState(false);
    let { userRoute, repoRoute } = useParams();
    let history = useHistory();

    useEffect(function() {
        fetch(`https://api.github.com/repos/${userRoute}/${repoRoute}/commits?per_page=20`)
            .then(response => { if (!response.ok) { return history.replace('/notfound'); } else { return response.json(); } })
            .then(data => setCommits(data))
            .catch(() => setFetchstatus(false))
    }, [userRoute, repoRoute, history])

    const loadnewpage = function() {
      fetch(`https://api.github.com/repos/${userRoute}/${repoRoute}/commits?per_page=20&page=${page+1}`)
            .then(response => response.json())
            .then(data => {
              if (data.length < 20) { setButtonoff(true) }
              setCommits(prev => [...prev, ...data])
            })
            .catch(() => setFetchstatus(false))

      setPage(prev => prev + 1);
    }
    return (
      <>
        <Nav />
        <div className="container">
          {fetchstatus && commits.length !== 0 ? 
            <>
                <h1>{repoRoute}</h1>
                <Commits data={commits} load={loadnewpage} buttonoff={buttonoff} />
            </>
          :
            'Something went wrong. Refresh the page to try again.'
          }
        </div>
      </>
    );
}

export default Repository;