import React, { useState, useEffect } from 'react';
import { useParams, useHistory } from "react-router-dom";

import Nav from '../../components/global/Nav';
import Icon from '../../components/profile/Icon';
import Repo from '../../components/profile/Repo';

function Profile() {
    const [ fetchstatus, setFetchstatus ] = useState(true);
    const [ repodata, setRepodata ] = useState([]);
    let { userRoute } = useParams();
    let history = useHistory();

    useEffect(function() {
        fetch(`https://api.github.com/users/${userRoute}/repos`)
            .then(response => { if (!response.ok) { return history.replace('/notfound'); } else { return response.json(); } })
            .then(data => setRepodata(data))
            .catch(() => setFetchstatus(false))
    }, [userRoute, history])

    return (
      <>
        <Nav />
        <div className="container">
          {fetchstatus ? 
            repodata.length !== 0 ?
            <>
              <div className="profile">
                <div className="profile__pfp"><Icon size="big" img={repodata[0].owner.avatar_url} alt={repodata[0].owner.login} /></div>
                <div className="profile__name"><h1>{repodata[0].owner.login}</h1></div>
              </div>

              <Repo data={repodata} />
            </>
            :
            'The user exists, but has no repositories.'
          :
            'Something went wrong. Refresh the page to try again.'
          }
        </div>
      </>
    );
}

export default Profile;