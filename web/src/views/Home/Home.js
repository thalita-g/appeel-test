import { useHistory } from "react-router-dom";

import Search from '../../components/form-fields/Search';

function Home() {
    let history = useHistory();

    const handleUserSearch = user => history.push('/' + user);

    return (
      <div className="container">
          <h1>Prettier Github</h1>
          <span className="subtitle">It's GitHub, but (questionably) prettier.</span>

          <p>This is part of a test where I make use of the GitHub API to showcase profiles and commits.</p>

          <p>Write a username below to make your first search.</p>

          <Search size="big" placeholder="Search GitHub user" handleSubmit={handleUserSearch} />
      </div>
    );
}

export default Home;