import { useHistory } from "react-router-dom";

import Search from '../../components/form-fields/Search';
import Nav from '../../components/global/Nav';

function NotFound() {
    let history = useHistory();

    const handleUserSearch = user => history.push('/' + user);

    return (
      <>
        <Nav />
        <div className="container">
            <p>Well, this is awkward. We couldn't find what you were looking for. Maybe try searching again?</p>
            <Search size="big" placeholder="Search GitHub user" handleSubmit={handleUserSearch} />
        </div>
      </>
    );
}

export default NotFound;