import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";

import Home from './views/Home/Home';
import Profile from './views/Profile/Profile';
import Repository from './views/Repository/Repository';
import NotFound from './views/NotFound/NotFound';

import './App.css';

function App() {
  return (
    <Router>
      <Switch>
        <Route path="/notfound" component={NotFound} />
        <Route path="/:userRoute/:repoRoute" component={Repository} />
        <Route path="/:userRoute" component={Profile} />
        <Route path="/" component={Home} />
      </Switch>
    </Router>
  );
}

export default App;