/// <reference types="cypress" />

context('Default actions', () => {  
    it('Type a user', () => {
      cy.visit('http://localhost:3000')
      cy.get('.search__input')
        // type a user
        .type('tbillington').should('have.value', 'tbillington')
  
        // type with special character sequences
        .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
        .type('{del}{selectall}{backspace}')
  
        // type with key modifiers
        .type('{alt}{option}') //these are equivalent
        .type('{ctrl}{control}') //these are equivalent
        .type('{meta}{command}{cmd}') //these are equivalent
        .type('{shift}')
  
        // Delay each keypress by 0.1 sec
        .type('tbillington', { delay: 100 })
        .should('have.value', 'tbillington')

        // Pass value
        .type('{enter}')

        cy.location().should((location) => {
          expect(location.href).to.eq('http://localhost:3000/tbillington')
        })

        // Try again but with clicking search button
        cy.visit('http://localhost:3000')
        cy.get('.search__input')
          .type('tbillington')

        cy.get('.search__button').click()
        cy.location().should((location) => {
          expect(location.href).to.eq('http://localhost:3000/tbillington')
        })
    })

    it('Sort repos', () => {
        cy.visit('http://localhost:3000/tbillington')

        // Sort test
        cy.get('.sort').click()
        cy.get('.sort__dropdown').should('be.visible')
        cy.contains('.sort__dropdown__item', 'Stars').click()

        // Enter repo
        cy.contains('.repository-list__single', 'kondo').click();
        cy.location().should((location) => {
          expect(location.href).to.eq('http://localhost:3000/tbillington/kondo')
        })
    })

    it('Navigate through commits', () => {
      cy.visit('http://localhost:3000/tbillington/kondo')

      // type in commit search
      cy.get('.commits .search__input')
          .type('add').should('have.value', 'add')
          .type('{leftarrow}{rightarrow}{uparrow}{downarrow}')
          .type('{del}{selectall}{backspace}')
          .type('{alt}{option}')
          .type('{ctrl}{control}')
          .type('{meta}{command}{cmd}')
          .type('{shift}')
          .type('add', { delay: 100 }).should('have.value', 'add')
          .type('{enter}')

      // search result component test
      cy.get('.commits__searchresults').should('be.visible')
      cy.contains('.commits__searchresults__result', 'Results for: add (only checks the 20 latest commits)').should('be.visible')

      // close search
      cy.get('.commits__searchresults__close').click()
      cy.get('.commits__searchresults').should('not.exist')

      // Try again but with clicking button
      cy.get('.commits .search__input').type('add')
      cy.get('.commits .search__button').click()

      cy.get('.commits__searchresults').should('be.visible')
      cy.contains('.commits__searchresults__result', 'Results for: add (only checks the 20 latest commits)').should('be.visible')

      // Commit descriptions containing "add" shouldn't exist
      cy.contains('.commits__single__desc', /^(?!.*add).*$/).should('not.exist')

      // Search string that'd return 0 commits
      cy.get('.commits .search__input').type('veryrandomstringthatnocommitdescriptionwouldeveruse').type('{enter}')
      cy.get('.commits__single__desc').should('not.exist')

      cy.get('.commits__searchresults__close').click()

      // Test load more button and fetching
      cy.get('.commits__loadmore').click().click().click().click().click().click()
      cy.get('.commits__single__desc').should('have.length', 140)
    })
  })
  